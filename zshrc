# ref: http://zanshin.net/2013/02/02/zsh-configuration-from-the-ground-up/

source ~/.zsh/functions.zsh
source ~/.zsh/options.zsh
source ~/.zsh/exports.zsh
source ~/.zsh/aliases.zsh
source ~/.zsh/bindkeys.zsh
source ~/.zsh/history.zsh
source ~/.zsh/colors.zsh
