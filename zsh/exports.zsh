# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# zsh-completions
# fpath=(/usr/local/share/zsh-completions $fpath)
fpath=(~/.oh-my-zsh/custom/plugins/zsh-completions $fpath)

# User configuration

export PATH=$HOME/bin:/usr/local/bin:$PATH
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh
export LC_ALL=zh_TW.UTF-8

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

#######################
##### old exports #####
#######################

export PATH="/usr/local/sbin:$PATH"

export TERM=xterm-256color

