# os x default
# export LSCOLORS="exfxcxdxbxegedabagacad"

# zsh default
# export LSCOLORS="Gxfxcxdxbxegedabagacad"

export LSCOLORS="gxfxcxdxcxegedabagacad"

# for ubuntu
# LS_COLORS="$LS_COLORS:di=36:ln=35:ex=32"