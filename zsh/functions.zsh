function set_zsh_title {
    emulate -L zsh

    if [ "$1" != "" ]; then
        ZSH_CUSTOM_TITLE=$1
    else
        ZSH_CUSTOM_TITLE=""
    fi

    source $ZSH/oh-my-zsh.sh
}

function reset_zsh_title {
    set_zsh_title
}
