# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# alias ls='ls -GF'
alias ll='ls -l'
alias cl='clear'
alias c='clear'

alias vi="vim"

# http://askubuntu.com/questions/22037/aliases-not-available-when-using-sudo
alias sudo='sudo '

