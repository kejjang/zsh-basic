# Clean, simple, compatible and meaningful.
# Tested on Linux, Unix and Windows under ANSI colors.
# It is recommended to use with a dark background.
# Colors: black, red, green, yellow, *blue, magenta, cyan, and white.
#
# Mar 2013 Yad Smood

# VCS
YS_VCS_PROMPT_PREFIX1=" %{$fg[white]%}on%{$reset_color%} "
YS_VCS_PROMPT_PREFIX2=":%{$fg[cyan]%}"
YS_VCS_PROMPT_SUFFIX="%{$reset_color%}"
YS_VCS_PROMPT_DIRTY=" %{$fg[red]%}x"
YS_VCS_PROMPT_CLEAN=" %{$fg[green]%}o"

# Git info
# local git_info='$(git_prompt_info)'
# ZSH_THEME_GIT_PROMPT_PREFIX="${YS_VCS_PROMPT_PREFIX1}git${YS_VCS_PROMPT_PREFIX2}"
# ZSH_THEME_GIT_PROMPT_SUFFIX="$YS_VCS_PROMPT_SUFFIX"
# ZSH_THEME_GIT_PROMPT_DIRTY="$YS_VCS_PROMPT_DIRTY"
# ZSH_THEME_GIT_PROMPT_CLEAN="$YS_VCS_PROMPT_CLEAN"

local exit_code="%(?,, C:%{$fg[red]%}%?%{$reset_color%})"

case $TERM in
    xterm*)
        if [ "$ZSH_CUSTOM_TITLE" != "" ]; then
            PR_TITLEBAR=$'%{\e]0;$ZSH_CUSTOM_TITLE\a%}'
        else
            PR_TITLEBAR=$'%{\e]0;%m\a%}'
        fi
        ;;
    screen)
        PR_TITLEBAR=$'%{\e_screen \005 (\005t) | %(!.-=[ROOT]=- | .)%n@%m:%~ | ${COLUMNS}x${LINES} | %y\e\\%}'
        ;;
    *)
        PR_TITLEBAR=''
        ;;
    esac

if [[ "$TERM" == "screen" ]]; then
    PR_STITLE=$'%{\ekzsh\e\\%}'
else
    PR_STITLE=''
fi

get_pwd_realsize() {
    local pwdsizelong=`echo -n $1 | wc -c | sed -e 's/^[[:space:]]*//'`
    local pwdsizeshort=${#1}
    local realsize
    let "realsize=($pwdsizeshort+$pwdsizelong)/2"
    echo $realsize
}

shorten_pwd() {
    local pwd_length=$(get_pwd_realsize $1)
    local pwd_lenmax=$2

    local split=(${(s:/:)1})
    local split_length=${#${split}}

    local shorten
    local short_part
    local part_length
    local part_short_len

    if [[ $split[1] = "~" ]]; then
        shorten="~"
    else
        part_length=${#${split[1]}}
        short_part=`echo $split[1] | cut -c1`
        part_short_len=1
        if [ "$short_part" = "." ]; then
            short_part=`echo $split[1] | cut -c1,2`
            part_short_len=2
        fi
        shorten="/"$short_part
        let "pwd_length=$pwd_length+$part_short_len-$part_length"
    fi

    for (( i = 2; i < $split_length; i++ )){
        if [[ $pwd_length -gt $pwd_lenmax ]]; then
            part_length=${#${split[$i]}}
            short_part=`echo $split[$i] | cut -c1`
            part_short_len=1
            if [ "$short_part" = "." ]; then
                short_part=`echo $split[$i] | cut -c1,2`
                part_short_len=2
            fi
            shorten=$shorten"/"$short_part
            let "pwd_length=$pwd_length+$part_short_len-$part_length"
        else
            shorten=$shorten"/"$split[$i]
        fi
    }
    shorten=$shorten"/"$split[-1]
    echo $shorten
}

theme_precmd () {
    local YS_THEME_PROMPT_EXITCODE=`print -P "%(?,, C:%?)"`
    local YS_THEME_PROMPT_EXITCODE_LENGTH=${#${YS_THEME_PROMPT_EXITCODE}}

    local YS_THEME_PROMPT_PATH_MAXLENGTH=0

    local YS_THEME_PROMPT_WINDOW_WIDTH=`tput cols`
    local YS_THEME_PROMPT_RESERVED=20

    local YS_THEME_PROMPT_N=`print -P %n`
    local YS_THEME_PROMPT_N_LENGTH=${#${YS_THEME_PROMPT_N}}

    local YS_THEME_PROMPT_M=`print -P %m`
    local YS_THEME_PROMPT_M_LENGTH=${#${YS_THEME_PROMPT_M}}

    ZSH_THEME_GIT_PROMPT_PREFIX=
    ZSH_THEME_GIT_PROMPT_SUFFIX=
    ZSH_THEME_GIT_PROMPT_DIRTY=
    ZSH_THEME_GIT_PROMPT_CLEAN=

    local YS_THEME_PROMPT_GIT_INFO=`git_prompt_info`
    local YS_THEME_PROMPT_GIT_INFO_LENGTH=${#${YS_THEME_PROMPT_GIT_INFO}}
    if [[ $YS_THEME_PROMPT_GIT_INFO_LENGTH > 0 ]];then
        (( YS_THEME_PROMPT_RESERVED=$YS_THEME_PROMPT_RESERVED+10 ))
    fi

    let "YS_THEME_PROMPT_PATH_MAXLENGTH=$YS_THEME_PROMPT_WINDOW_WIDTH-$YS_THEME_PROMPT_RESERVED-$YS_THEME_PROMPT_N_LENGTH-$YS_THEME_PROMPT_M_LENGTH-$YS_THEME_PROMPT_GIT_INFO_LENGTH-$YS_THEME_PROMPT_EXITCODE_LENGTH"

    PR_PWD=${(%):-%~}
    local pwdsize=$(get_pwd_realsize $PR_PWD)

    if [[ $pwdsize -gt $YS_THEME_PROMPT_PATH_MAXLENGTH ]]; then
        PR_PWD=$(shorten_pwd $PR_PWD $YS_THEME_PROMPT_PATH_MAXLENGTH)
        pwdsize=$(get_pwd_realsize $PR_PWD)
    fi

    ZSH_THEME_GIT_PROMPT_PREFIX="${YS_VCS_PROMPT_PREFIX1}git${YS_VCS_PROMPT_PREFIX2}"
    ZSH_THEME_GIT_PROMPT_SUFFIX="$YS_VCS_PROMPT_SUFFIX"
    ZSH_THEME_GIT_PROMPT_DIRTY="$YS_VCS_PROMPT_DIRTY"
    ZSH_THEME_GIT_PROMPT_CLEAN="$YS_VCS_PROMPT_CLEAN"
}

# Prompt format:
#
# PRIVILEGES USER @ MACHINE in DIRECTORY on git:BRANCH STATE [TIME] C:LAST_EXIT_CODE
# $ COMMAND
#
# For example:
#
# % ys @ ys-mbp in ~/.oh-my-zsh on git:master x [21:47:42] C:0
# $
PROMPT='$PR_STITLE${(e)PR_TITLEBAR}\
$'\n'
%{$terminfo[bold]$fg[blue]%}#%{$reset_color%} \
%(#,%{$bg[yellow]%}%{$fg[black]%}%n%{$reset_color%},%{$fg[cyan]%}%n) \
%{$fg[white]%}@ \
%{$fg[green]%}%m \
%{$fg[white]%}in \
%{$terminfo[bold]$fg[yellow]%}$PR_PWD%{$reset_color%}\
`git_prompt_info`\
 \
%{$fg[white]%}[%D{%H:%M:%S}]$exit_code
%{$terminfo[bold]$fg[red]%}$ %{$reset_color%}'

autoload -U add-zsh-hook
add-zsh-hook precmd  theme_precmd
